#!/usr/bin/env python

import argparse
import json
import logging
import pathlib
from typing import Union

from scim2_client import (
    Config,
    Client,
    user_query_request,
    ScimError,
    ErrorResponse,
    ListResponse,
    User,
    QueryParameters,
)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--parameter",
        metavar=("NAME", "VALUE"),
        help="multiple parameters are allowed",
        action="append",
        nargs=2,
    )
    parser.add_argument(
        "-c",
        "--config",
        help="path to configuration file (JSON)",
        type=pathlib.Path,
        default="config.json",
    )
    parser.add_argument(
        "-s",
        "--count",
        help="How many resources to fetch per request",
        type=pathlib.Path,
    )
    args = parser.parse_args()

    logging.basicConfig(level=logging.NOTSET)
    with open(args.config, "r", encoding="utf-8") as f:
        client = Client(config=Config(**json.load(f)))

    params = {k: v for k, v in args.parameter} if args.parameter else {}
    response: Union[ListResponse[User], ErrorResponse]
    try:
        num_responses = 0
        for response in client.iter_list_response(
            user_query_request(
                parameters=QueryParameters(count=args.count),
                # SCIM allows non-standard query parameters.  The server should ignore
                # what it doesn't understand.
                extra_parameters=params,
            )
        ):
            print(response.json(exclude_unset=True, by_alias=True))
            num_responses += 1
        # We're done
        print('{"num_responses":', num_responses, "}")

    except ScimError as exc:
        print(exc.response.json(exclude_unset=True, by_alias=True))
        exit(1)


if __name__ == "__main__":
    main()
