#!/usr/bin/env python

import argparse
import json
import logging
import pathlib
from typing import Union

from scim2_client import (
    Config,
    Client,
    user_query_request,
    ScimError,
    ErrorResponse,
    ListResponse,
    User,
)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--parameter",
        metavar=("NAME", "VALUE"),
        help="multiple parameters are allowed",
        action="append",
        nargs=2,
    )
    parser.add_argument(
        "-c",
        "--config",
        help="path to configuration file (JSON)",
        type=pathlib.Path,
        default="config.json",
    )
    args = parser.parse_args()

    logging.basicConfig(level=logging.NOTSET)
    with open(args.config, "r", encoding="utf-8") as f:
        client = Client(config=Config(**json.load(f)))

    params = {k: v for k, v in args.parameter} if args.parameter else {}
    if "count" not in params:
        params["count"] = 5  # Return at most 5 resources
    if "startIndex" not in params:
        params["startIndex"] = 1  # Starting with the first

    response: Union[ListResponse[User], ErrorResponse]
    try:
        response = client(
            user_query_request(
                # SCIM allows non-standard query parameters.  The server should ignore
                # what it doesn't understand.
                extra_parameters=params,
            )
        ).payload
    except ScimError as exc:
        response = exc.response

    print(response.json(exclude_unset=True, by_alias=True))

    if isinstance(response, ErrorResponse):
        exit(1)


if __name__ == "__main__":
    main()
