#!/usr/bin/env python

import argparse
import datetime
import json
import logging
import pathlib
from typing import Union

from scim2_client import (
    Config,
    Client,
    user_query_request,
    QueryParameters,
    AttributesParameter,
    eq,
    ge,
    ScimError,
    ErrorResponse,
    ListResponse,
    User,
)


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        help="path to configuration file (JSON)",
        type=pathlib.Path,
        default="config.json",
    )
    parser.add_argument(
        "-a",
        "--attributes",
        nargs="*",
    )
    args = parser.parse_args()

    logging.basicConfig(level=logging.NOTSET)
    with open(args.config, "r", encoding="utf-8") as f:
        client = Client(config=Config(**json.load(f)))

    attributes = AttributesParameter(attributes=args.attributes) if args.attributes else None

    is_active = eq("active", True)
    one_week_ago = datetime.datetime.fromordinal(
        # Use date() to round value, SCIM only uses DateTime for dates
        (datetime.datetime.now() - datetime.timedelta(weeks=1))
        .date()
        .toordinal()
    )
    recently_modified = ge("meta.lastModified", one_week_ago)
    query_filter = is_active.and_(recently_modified)
    response: Union[ListResponse[User], ErrorResponse]
    try:
        response = client(
            user_query_request(
                parameters=QueryParameters(
                    count=5,  # Return at most 5 resources
                    start_index=1,  # Starting with the first
                    attributes=attributes,  # Return only these attributes
                    filter=query_filter,  # Use this filter
                )
            )
        ).payload
    except ScimError as exc:
        response = exc.response

    print(response.json(exclude_unset=True, by_alias=True))

    if isinstance(response, ErrorResponse):
        exit(1)


if __name__ == "__main__":
    main()
