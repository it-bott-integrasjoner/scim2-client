"""SCIM client module."""

from __future__ import annotations

import dataclasses
import logging
from typing import Dict, TypeVar, Optional, TYPE_CHECKING, Mapping, Any, Iterator, Generic

import requests
from pydantic import (
    BaseModel,
    Extra,
    SecretStr,
    parse_obj_as,
    ValidationError,
    validator,
    HttpUrl,
    constr,
    Field,
)

from scim2_client.scim.request import (
    ItemRequest,
    ResourceRequest,
    QueryRequest,
    Request,
    QueryParameters,
)
from scim2_client.scim.response import ErrorResponse, ListResponse, ResourceT

__all__ = [
    "Client",
    "Config",
    "Endpoints",
    "ImmutableModel",
    "ResourceResponse",
    "ScimError",
]


logger = logging.getLogger(__name__)

T = TypeVar("T")


class ImmutableModel(BaseModel):
    """Base class for client configuration.

    Disallows extra fields and mutation to prevent silent errors when
    configuration format is changed.

    """

    class Config:  # noqa: D106
        extra = Extra.forbid
        allow_mutation = False


if TYPE_CHECKING:  # pragma: no cover
    EndpointPath = str
else:
    EndpointPath = constr(regex=r"^[^./]")


class Endpoints(ImmutableModel):
    """SCIM endpoints."""

    base_url: HttpUrl
    users: EndpointPath = "Users"
    groups: EndpointPath = "Groups"

    @validator("base_url")
    def _check_base_url(cls, url: HttpUrl) -> HttpUrl:
        if url.fragment:
            raise ValueError("fragment not allowed")
        if url.query:
            raise ValueError("query not allowed")
        if url.path is not None and url.path.endswith("/"):
            raise ValueError("path must not end with /")
        return url

    @property
    def users_url(self) -> HttpUrl:
        """Build URL to Users endpoint."""
        return parse_obj_as(HttpUrl, f"{self.base_url}/{self.users}")

    @property
    def groups_url(self) -> HttpUrl:
        """Build URL to Groups endpoint."""
        return parse_obj_as(HttpUrl, f"{self.base_url}/{self.groups}")


class Config(ImmutableModel):
    """SCIM client configuration."""

    endpoints: Endpoints
    headers: Dict[str, SecretStr] = Field(default_factory=dict)


class ScimError(Exception):
    """SCIM error.

    Turns a SCIM error response into a python exception.
    """

    def __init__(self, response: ErrorResponse, request: Request) -> None:
        """Initialize.

        :param ErrorResponse response:  The SCIM error response

        """
        super().__init__(response.detail)
        self.response = response
        self.request = request


class UnhandledResponseError(AssertionError):
    pass


def _error_json(response: requests.Response) -> Any:
    if 400 <= response.status_code < 600:  # pragma: no branch
        try:
            return response.json()
        except Exception:  # noqa
            pass
    response.raise_for_status()
    raise ValueError("Not an error response")  # pragma: no cover


@dataclasses.dataclass(frozen=True)
class ResourceResponse(Generic[T]):
    """Wraps payload and request."""

    payload: T
    request: ResourceRequest[T]

    @property
    def next_request(self) -> Optional[ResourceRequest[T]]:
        """Get next page request for supported requests and payloads.

        :raises NotImplementedError: for unsupported requests or payloads
        """
        request = self.request
        payload = self.payload
        if not isinstance(request, QueryRequest):
            raise NotImplementedError(f"Not implemented for request type: {type(request)}")
        if not isinstance(payload, ListResponse):
            raise NotImplementedError(f"Not implemented for payload type: {type(payload)}")
        if payload.total_results == 0:
            return None
        if not payload.start_index:
            return None
        resource_count = len(payload.resources)
        if resource_count < 1:
            return None
        next_index = resource_count + payload.start_index
        if next_index > payload.total_results:
            return None
        return dataclasses.replace(
            request,
            parameters=dataclasses.replace(
                request.parameters or QueryParameters(),
                start_index=next_index,
            ),
        )


class Client:
    """SCIM client."""

    def __init__(self, *, config: Config, session: Optional[requests.Session] = None) -> None:
        """Initialize.

        :param Config config:
        :param Optional[requests.Session] session:
            If you need more advanced session handling than the default.
            You may for instance want the client to automatically retry
            failed requests.
        """
        if not isinstance(config, Config):
            raise TypeError
        self.config = config
        self.session = session or requests.Session()
        self.urls: Mapping[Request.Type, HttpUrl] = {
            Request.Type.USER: self.config.endpoints.users_url,
            Request.Type.GROUP: self.config.endpoints.groups_url,
        }

    def __call__(self, request: ResourceRequest[T]) -> ResourceResponse[T]:
        """Call SCIM server and return a response."""
        params = request.query_params if isinstance(request, QueryRequest) else None
        method_name = request.method
        if isinstance(request, ItemRequest):
            url = f"{self.urls[request.request_type]}/{request.id}"
        else:
            url = self.urls[request.request_type]
        logger.debug(
            "Calling %s %s with params=%r, headers=%r",
            method_name,
            url,
            params,
            self.config.headers,
        )
        response = self.session.request(
            method_name,
            url,
            headers={k: v.get_secret_value() for k, v in self.config.headers.items()},
            params=params,
        )
        if isinstance(response, request.result_type):
            return ResourceResponse(request=request, payload=response)
        if response.status_code == 200:
            return ResourceResponse(
                request=request,
                payload=parse_obj_as(request.result_type, response.json()),
            )

        if 400 <= response.status_code < 600:
            try:
                raise ScimError(
                    response=parse_obj_as(ErrorResponse, _error_json(response)),
                    request=request,
                )
            except ValidationError as exc:
                logger.error("Failed to parse error response", exc_info=exc)
            response.raise_for_status()

        raise UnhandledResponseError(f"Unhandled response, status code {response.status_code}")

    def iter_list_response(
        self, request: QueryRequest[ListResponse[ResourceT]]
    ) -> Iterator[ListResponse[ResourceT]]:
        """Generate a stream of list responses.

        See :py:meth:`ResourceResponse.next_request`
        """
        response = self(request)
        while True:
            if not isinstance(response.payload, ListResponse):  # pragma: no cover
                raise AssertionError(f"Expected ListResponse, got {type(response.payload)}")
            yield response.payload
            if not response.next_request:
                return
            response = self(response.next_request)

    def iter_resources(self, request: QueryRequest[ListResponse[ResourceT]]) -> Iterator[ResourceT]:
        """Generate a stream of resource objects."""
        for response in self.iter_list_response(request):
            for x in response.resources:
                yield x
