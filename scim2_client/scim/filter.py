"""A simple dsl to create SCIM filters.

- https://datatracker.ietf.org/doc/html/rfc7644#section-3.4.2.2
- https://ldapwiki.com/wiki/SCIM%20Filtering

"""

from __future__ import annotations

import abc
import dataclasses
import datetime
import json
from typing import Union

from scim2_client.scim.json import ScimJSONEncoder

__all__ = [
    "And",
    "AttrExpr",
    "Compare",
    "Contains",
    "EndsWith",
    "Equal",
    "Filter",
    "FilterBaseModel",
    "GreaterThan",
    "GreaterThanOrEqual",
    "Group",
    "LessThan",
    "LessThanOrEqual",
    "LogExpr",
    "Not",
    "NotEqual",
    "Or",
    "Present",
    "StartsWith",
    "ValuePath",
    "co",
    "eq",
    "ew",
    "ge",
    "gt",
    "le",
    "lt",
    "ne",
    "pr",
    "sw",
]


class FilterBaseModel(abc.ABC):
    """Base class for SCIM filters."""

    @abc.abstractmethod
    def __str__(self) -> str:
        """Produce a string to be used in an SCIM filter query."""


@dataclasses.dataclass(frozen=True)
class Filter(FilterBaseModel):
    """Corresponds to FILTER in SCIM filter ABNF."""

    expression: Union[Filter, LogExpr, ValuePath, AttrExpr, Not]

    def and_(self, b: Union[Filter, LogExpr, ValuePath, AttrExpr, Not]) -> Filter:
        """See :py:class:`And`."""
        if not isinstance(b, Filter):
            b = Filter(expression=b)
        return Filter(expression=And(first=self, second=b))

    def or_(self, b: Union[Filter, LogExpr, ValuePath, AttrExpr, Not]) -> Filter:
        """See :py:class:`Or`."""
        if not isinstance(b, Filter):
            b = Filter(expression=b)
        return Filter(expression=Or(first=self, second=b))

    def group(self) -> Filter:
        """See :py:class:`Group`."""
        return Group(expression=self)

    def not_(self) -> Filter:
        """See :py:class:`Not`."""
        return Filter(expression=Not(expression=self))

    def value_path(self, name: str) -> Filter:
        """See :py:class:`ValuePath`."""
        return Filter(
            expression=ValuePath(
                name=name,
                filter=self,
            )
        )

    def __str__(self) -> str:  # noqa: D105
        return str(self.expression)


@dataclasses.dataclass(frozen=True)
class Group(Filter):
    """Precedence grouping.

    Boolean expressions MAY be grouped using parentheses to change the
    standard order of operations, i.e., to evaluate logical "or"
    operators before logical "and" operators.

    """

    def __str__(self) -> str:
        """Produce a string to be used in an SCIM filter query."""
        return f"({self.expression})"


@dataclasses.dataclass(frozen=True)  # type: ignore[misc]
class AttrExpr(FilterBaseModel, abc.ABC):
    """Base class for operators that take an attribute path."""

    name: str

    @property
    @abc.abstractmethod
    def op(self) -> str:
        """Produce a string to be used in an SCIM filter query."""


@dataclasses.dataclass(frozen=True)
class Present(AttrExpr):
    """Present.

    If the attribute has a non-empty or non-null value, or if it
    contains a non-empty node for complex attributes, there is a
    match.

    """

    def __str__(self) -> str:  # noqa: D105
        return f"{self.name} {self.op}"

    @property
    def op(self) -> str:  # noqa: D102
        return "pr"


@dataclasses.dataclass(frozen=True)
class ValuePath(FilterBaseModel):
    """Complex attribute filter grouping.

    Service providers MAY support complex filters where expressions
    MUST be applied to the same value of a parent attribute specified
    immediately before the left square bracket ("[").  The expression
    within square brackets ("[" and "]") MUST be a valid filter
    expression based upon sub-attributes o the parent attribute.
    Nested expressions MAY be used.  See examples below.

    """

    name: str
    filter: Filter

    def __str__(self) -> str:  # noqa: D105
        return f"{self.name}[{self.filter}]"


CompValue = Union[None, bool, int, float, str, datetime.datetime]


@dataclasses.dataclass(frozen=True)  # type: ignore[misc]
class Compare(AttrExpr, abc.ABC):
    """Base class for comparison operators."""

    value: CompValue

    def __str__(self) -> str:  # noqa: D105
        value = json.dumps(self.value, cls=ScimJSONEncoder)
        return f"{self.name} {self.op} {value}"


@dataclasses.dataclass(frozen=True)
class Equal(Compare):
    """Equal.

    The attribute and operator values must be identical for a match.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "eq"


@dataclasses.dataclass(frozen=True)
class NotEqual(Compare):
    """Not equal.

    The attribute and operator values are not identical.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "ne"


@dataclasses.dataclass(frozen=True)
class Contains(Compare):
    """Contains.

    The entire operator value must be a substring of the attribute
    value for a match.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "co"


@dataclasses.dataclass(frozen=True)
class StartsWith(Compare):
    """Starts with.

    The entire operator value must be a substring of the attribute
    value, starting at the beginning of the attribute value.  This
    criterion is satisfied if the two strings are identical.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "sw"


@dataclasses.dataclass(frozen=True)
class EndsWith(Compare):
    """EndsWith.

    The entire operator value must be a substring of the attribute
    value, matching at the end of the attribute value.  This criterion
    is satisfied if the two strings are identical.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "ew"


@dataclasses.dataclass(frozen=True)
class GreaterThan(Compare):
    """GreaterThan.

    If the attribute value is greater than the operator value, there
    is a match.  The actual comparison is dependent on the attribute
    type.  For string attribute types, this is a lexicographical
    comparison, and for DateTime types, it is a chronological
    comparison.  For integer attributes, i is a comparison by numeric
    value.  Boolean and Binary attributes SHALL cause a failed
    response (HTTP status code 400) with "scimType" of
    "invalidFilter".

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "gt"


@dataclasses.dataclass(frozen=True)
class GreaterThanOrEqual(Compare):
    """GreaterThanOrEqual.

    If the attribute value is greater than or equal to the operator
    value, there is a match.  The actual comparison is dependent on
    the attribute type.  For string attribute types, this is a
    lexicographical comparison, and for DateTime types, it is a
    chronological comparison.  For integer attributes, it is a
    comparison by numeric value.  Boolean and Binary attributes SHALL
    cause a failed response (HTTP status code 400) with "scimType" of
    "invalidFilter".

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "ge"


@dataclasses.dataclass(frozen=True)
class LessThan(Compare):
    """LessThan.

    If the attribute value is less than the operator value, there is a
    match.  The actual comparison is dependent on the attribute type.
    For string attribute types, this is a lexicographical comparison,
    and for DateTime types, it is a chronological comparison.  For
    integer attributes, it is a comparison by numeric value.  Boolean
    and Binary attributes SHALL cause a failed response (HTTP status
    code 400) with "scimType" of "invalidFilter".

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "lt"


@dataclasses.dataclass(frozen=True)
class LessThanOrEqual(Compare):
    """LessThanOrEqual.

    If the attribute value is less than or equal to the operator
    value, there is a match.  The actual comparison is dependent on
    the attribute type.  For string attribute types, this is a
    lexicographical comparison, and for DateTime types, it is a
    chronological comparison.  For integer attributes, it is a
    comparison by numeric value.  Boolean and Binary attributes SHALL
    cause a failed response (HTTP status code 400) with "scimType" of
    "invalidFilter".

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "le"


@dataclasses.dataclass(frozen=True)  # type: ignore[misc]
class LogExpr(FilterBaseModel, abc.ABC):
    """Base class of logical expression."""

    first: Filter
    second: Filter

    @property
    @abc.abstractmethod
    def op(self) -> str:
        """Produce a string to be used in an SCIM filter query."""

    def __str__(self) -> str:  # noqa: D105
        return f"{(self.first)} {self.op} {self.second}"


@dataclasses.dataclass(frozen=True)
class And(LogExpr):
    """Logical and.

    The filter is only a match if both expressions evaluate to true.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "and"


@dataclasses.dataclass(frozen=True)
class Or(LogExpr):
    """Logical or.

    The filter is a match if either expression evaluates to true.

    """

    @property
    def op(self) -> str:  # noqa: D102
        return "or"


@dataclasses.dataclass(frozen=True)
class Not(FilterBaseModel):
    """Not function.

    The filter is a match if the expression evaluates to false.

    """

    expression: Filter

    def __str__(self) -> str:  # noqa: D105
        return f"not{Group(expression=self.expression)}"


def eq(name: str, value: CompValue) -> Filter:
    """See :py:class:`Equal`."""
    return Filter(expression=Equal(name=name, value=value))


def ne(name: str, value: CompValue) -> Filter:
    """See :py:class:`NotEqual`."""
    return Filter(expression=NotEqual(name=name, value=value))


def sw(name: str, value: str) -> Filter:
    """See :py:class:`StartsWith`."""
    return Filter(expression=StartsWith(name=name, value=value))


def ew(name: str, value: str) -> Filter:
    """See :py:class:`EndsWith`."""
    return Filter(expression=EndsWith(name=name, value=value))


def pr(name: str) -> Filter:
    """See :py:class:`Present`."""
    return Filter(expression=Present(name=name))


def lt(name: str, value: CompValue) -> Filter:
    """See :py:class:`LessThan`."""
    return Filter(expression=LessThan(name=name, value=value))


def le(name: str, value: CompValue) -> Filter:
    """See :py:class:`LessThanOrEqual`."""
    return Filter(expression=LessThanOrEqual(name=name, value=value))


def gt(name: str, value: CompValue) -> Filter:
    """See :py:class:`GreaterThan`."""
    return Filter(expression=GreaterThan(name=name, value=value))


def ge(name: str, value: CompValue) -> Filter:
    """See :py:class:`GreaterThanOrEqual`."""
    return Filter(expression=GreaterThanOrEqual(name=name, value=value))


def co(name: str, value: str) -> Filter:
    """See :py:class:`Contains`."""
    return Filter(expression=Contains(name=name, value=value))
