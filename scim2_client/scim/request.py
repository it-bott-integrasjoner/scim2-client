"""SCIM HTTP requests."""

from __future__ import annotations

import abc
import dataclasses
import datetime
import enum
import logging
from typing import (
    Optional,
    Dict,
    Any,
    Union,
    TypeVar,
    Generic,
    Iterable,
    Type,
)

from scim2_client.scim.filter import Filter, ValuePath

__all__ = [
    "AttributesParameter",
    "ItemRequest",
    "QueryParameters",
    "QueryRequest",
    "Request",
    "ResourceRequest",
]

from scim2_client.scim.json import format_datetime

logger = logging.getLogger(__name__)

T = TypeVar("T", bound=object)


@dataclasses.dataclass(frozen=True)
class ImmutableModel:
    """Immutable base model for SCIM HTTP requests."""


def aliases(class_or_instance: ImmutableModel) -> Dict[str, str]:
    """Get field names, use alias if field.metadata["alias"] is set."""

    def get_name(f: dataclasses.Field) -> str:
        try:
            return f.metadata["alias"]
        except KeyError:
            return f.name

    return {x.name: get_name(x) for x in dataclasses.fields(class_or_instance)}


@dataclasses.dataclass(frozen=True)
class AttributesParameter(ImmutableModel):
    """Attributes query parameter.

    The following attributes control which attributes SHALL be returned
    with a returned resource.  SCIM clients MAY use one of these two
    OPTIONAL parameters, which MUST be supported by SCIM service
    providers:

    attributes
      A multi-valued list of strings indicating the names of resource
      attributes to return in the response, overriding the set of
      attributes that would be returned by default.  Attribute names
      MUST be in standard attribute notation (Section 3.10) form.  See
      Section 3.9 for additional retrieval query parameters.

    excludedAttributes
      A multi-valued list of strings indicating the names of resource
      attributes to be removed from the default set of attributes to
      return.  This parameter SHALL have no effect on attributes whose
      schema "returned" setting is "always" (see Sections 2.2 and 7 of
      [RFC7643]).  Attribute names MUST be in standard attribute
      notation (Section 3.10) form.  See Section 3.9 for additional
      retrieval query parameters.

    See https://datatracker.ietf.org/doc/html/rfc7644#section-3.4.2.5

    """

    attributes: Iterable[str] = frozenset()
    include: bool = True

    @property
    def query_parameter_name(self) -> str:
        """Query parameter name for HTTP request."""
        return "attributes" if self.include else "excludedAttributes"


@dataclasses.dataclass(frozen=True)
class QueryParameters(ImmutableModel):
    """Query parameters for SCIM resource queries.

    The SCIM protocol defines a standard set of query parameters that
    can be used to filter, sort, and paginate to return zero or more
    resources in a query response.  Queries MAY be made against a
    single resource or a resource type endpoint (e.g., "/Users"), or
    the service provider Base URI.  SCIM service providers MAY support
    additional query parameters not specified here and SHOULD ignore
    any query parameters they do not recognize instead of rejecting
    the query for versioning compatibility reasons.


    See https://datatracker.ietf.org/doc/html/rfc7644#section-3.4.2

    """

    class SortOrder(str, enum.Enum):  # noqa: D106
        ASCENDING = "ascending"
        DESCENDING = "descending"

    filter: Union[None, Filter, ValuePath] = None
    count: Optional[int] = None
    attributes: Optional[AttributesParameter] = None
    start_index: Optional[int] = dataclasses.field(default=None, metadata={"alias": "startIndex"})
    sort_by: Optional[str] = dataclasses.field(default=None, metadata={"alias": "sortBy"})
    sort_order: Optional[QueryParameters.SortOrder] = dataclasses.field(
        default=None, metadata={"alias": "sortOrder"}
    )

    @property
    def dict(self) -> Dict[str, Any]:
        """Create a dictionary of HTTP query parameters."""
        params: Dict[str, Any] = {}
        property_aliases = aliases(self)
        if self.filter is not None:
            params["filter"] = str(self.filter)
        if self.count is not None:
            params["count"] = self.count
        if self.start_index is not None:
            params[property_aliases["start_index"]] = self.start_index
        if self.attributes is not None:
            params[self.attributes.query_parameter_name] = ",".join(
                str(x) for x in self.attributes.attributes
            )
        if self.sort_by is not None:
            params[property_aliases["sort_by"]] = self.sort_by
        if self.sort_order is not None:
            params[property_aliases["sort_order"]] = self.sort_order
        return params


@dataclasses.dataclass(frozen=True)
class Request(Generic[T], ImmutableModel, abc.ABC):
    """SCIM HTTP request base class."""

    class Type(enum.Enum):
        """Used to tell which endpoint to use."""

        USER = enum.auto()
        GROUP = enum.auto()

    request_type: Request.Type

    def __post_init__(self) -> None:
        """Post init validation."""
        if not type(self.request_type) == self.__class__.Type:
            raise TypeError(f"Invalid request type '{self.request_type}'")

    @property
    def method(self) -> str:
        """Return method to be used in HTTP request."""
        return "GET"


@dataclasses.dataclass(frozen=True)
class ResourceRequest(Request[T], abc.ABC):
    """Base class for requests that return SCIM resources."""

    result_type: Type[T]

    def __post_init__(self) -> None:
        """Validate data."""
        super().__post_init__()
        if not self.result_type:
            raise TypeError("result_type can't be empty")


@dataclasses.dataclass(frozen=True)
class QueryRequest(ResourceRequest[T]):
    """SCIM query request.

    See https://datatracker.ietf.org/doc/html/rfc7644#section-3.4.2

    """

    parameters: Optional[QueryParameters] = None
    extra_parameters: Optional[Dict[str, Any]] = None

    @property
    def query_params(self) -> Dict[str, Any]:
        """Create a dictionary of HTTP query parameters."""
        if self.extra_parameters:
            extra_parameters = {
                k: self._encode_extra_parameter(v) for k, v in self.extra_parameters.items()
            }
        else:
            extra_parameters = {}
        if self.parameters is None:
            return extra_parameters
        params: Dict[str, Any] = extra_parameters
        return {**params, **self.parameters.dict}

    def _encode_extra_parameter(self, value: Any) -> str:
        if isinstance(value, bool):
            if value:
                return "true"
            return "false"
        if isinstance(value, datetime.datetime):
            return format_datetime(value)
        return value


@dataclasses.dataclass(frozen=True)
class ItemRequest(ResourceRequest[T]):
    """Base class for get SCIM resource by id requests."""

    id: str
    attributes: Optional[AttributesParameter] = None

    def __post_init__(self) -> None:
        """Validate data."""
        super().__post_init__()
        if not self.id:
            raise ValueError("id cannot be empty")
