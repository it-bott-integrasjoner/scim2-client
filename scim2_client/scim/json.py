"""Utilities for serializing SCIM values to JSON."""
from __future__ import annotations

import datetime
import json
from typing import Any

__all__ = ["ScimJSONEncoder", "format_datetime"]


def format_datetime(value: datetime.datetime) -> str:
    """Convert datetime to SCIM format."""
    return value.astimezone(datetime.timezone.utc).strftime("%Y-%m-%dT%H:%M:%SZ")


class ScimJSONEncoder(json.JSONEncoder):  # noqa: D101
    def default(self, o: Any) -> Any:  # noqa: D102
        if isinstance(o, datetime.datetime):
            return format_datetime(o)
        return super().default(o)
