"""SCIM response model classes."""

from __future__ import annotations

import abc
import datetime
import enum
from typing import (
    Optional,
    Union,
    Sequence,
    FrozenSet,
    Any,
    TypeVar,
    Generic,
)

import pydantic
from pydantic import Extra, Field

from scim2_client.scim.json import ScimJSONEncoder

__all__ = [
    "BaseModel",
    "ErrorResponse",
    "ErrorType",
    "ListResponse",
    "Resource",
    "ResourceT",
    "Response",
]


class BaseModel(pydantic.BaseModel, abc.ABC):
    """Basemodel for responses.

    Models are immutable, uses aliases to get PEP8 compliant attribute
    names and allows extra fields

    """

    class Config:  # noqa: D106
        allow_mutation = False
        allow_population_by_field_name = True
        extra = Extra.allow
        json_encoders = {datetime.date: ScimJSONEncoder().default}


class Response(BaseModel, abc.ABC):
    """SCIM response base class."""

    pass


def _fix_str_schemas(value: Any) -> FrozenSet[str]:
    # Workaround for implementations that return a string instead of
    # a singleton array of strings.
    if isinstance(value, str):
        value = {value}
    return pydantic.parse_obj_as(FrozenSet[str], value)


class ErrorType(str, enum.Enum):
    """Used in error responses."""

    INVALID_FILTER = "invalidFilter"
    TOO_MANY = "tooMany"
    UNIQUENESS = "uniqueness"
    MUTABILITY = "mutability"
    INVALID_SYNTAX = "invalidSyntax"
    INVALID_PATH = "invalidPath"
    NO_TARGET = "noTarget"
    INVALID_VALUE = "invalidValue"
    INVALID_VERS = "invalidVers"
    SENSITIVE = "sensitive"


class ErrorResponse(Response):
    """HTTP Status and Error Response Handling.

    The SCIM protocol uses the HTTP response status codes defined in
    Section 6 of [RFC7231] to indicate operation success or failure.
    In addition to returning an HTTP response code, implementers MUST
    return the errors in the body of the response in a JSON format,
    using the attributes described below.  Error responses are
    identified using the following "schema" URI:
    "urn:ietf:params:scim:api:messages:2.0:Error".  The following
    attributes are defined for a SCIM error response using a JSON
    body:

    status
       The HTTP status code (see Section 6 of [RFC7231]) expressed as
       a JSON string.  REQUIRED.

    scimType
       A SCIM detail error keyword.  See :py:class:`ErrorType`.
       OPTIONAL.

    detail
       A detailed human-readable message.  OPTIONAL.

    See https://datatracker.ietf.org/doc/html/rfc7644#section-3.12

    """

    schemas: FrozenSet[str]
    status: str
    scim_type: Union[ErrorType, str, None] = Field(alias="scimType")
    detail: Optional[str]

    @pydantic.validator("schemas", pre=True)
    def check_schemas(cls, value: FrozenSet[str]) -> FrozenSet[str]:  # noqa: D102
        value = _fix_str_schemas(value)
        if "urn:ietf:params:scim:api:messages:2.0:Error" not in value:
            raise ValueError("missing schema")
        return value


class Meta(BaseModel):
    """Meta attribute.

    A complex attribute containing resource metadata.  All "meta"
    sub-attributes are assigned by the service provider (have a
    "mutability" of "readOnly"), and all of these sub-attributes have
    a "returned" characteristic of "default".  This attribute SHALL be
    ignored when provided by clients.  "meta" contains the following
    sub-attributes:

    resourceType
       The name of the resource type of the resource.  This attribute
       has a mutability of "readOnly" and "caseExact" as "true".

    created
       The "DateTime" that the resource was added to the service
       provider.  This attribute MUST be a DateTime.

    lastModified
       The most recent DateTime that the details of this resource were
       updated at the service provider.  If this resource has never
       been modified since its initial creation, the value MUST be the
       same as the value of "created".

    location
       The URI of the resource being returned.  This value MUST be the
       same as the "Content-Location" HTTP response header (see
       Section 3.1.4.2 of [RFC7231]).

    version
       The version of the resource being returned.  This value must be
       the same as the entity-tag (ETag) HTTP response header (see
       Sections 2.1 and 2.3 of [RFC7232]).  This attribute has
       "caseExact" as "true".  Service provider support for this
       attribute is optional and subject to the service provider's
       support for versioning (see Section 3.14 of [RFC7644]).  If a
       service provider provides "version" (entity-tag) for a
       representation and the generation of that entity-tag does not
       satisfy all of the characteristics of a strong validator (see
       Section 2.1 of [RFC7232]), then the origin server MUST mark the
       "version" (entity-tag) as weak by prefixing its opaque value
       with "W/" (case sensitive).

    """

    resource_type: Optional[str] = Field(alias="resourceType")
    created: Optional[datetime.datetime]
    last_modified: Optional[datetime.datetime] = Field(alias="lastModified")
    location: Optional[str]
    version: Optional[str]


class Resource(Response):
    """A single SCIM resource.

    See for example :py:class:`scim2_client.User`.
    """

    schemas: Optional[FrozenSet[str]]
    id: str
    meta: Optional[Meta]

    class Config:
        """Model configuration.

        valid_resource_type
            If set, check that meta.resourceType equals this value
        """

        valid_resource_type: Optional[str] = None

    @pydantic.validator("schemas", pre=True)
    def check_schemas(cls, value: FrozenSet[str]) -> FrozenSet[str]:  # noqa: D102
        return _fix_str_schemas(value)

    @pydantic.validator("meta")
    def check_resource_type(cls, value: Meta) -> Optional[Meta]:  # noqa: D102
        valid_type = cls.Config.valid_resource_type
        if valid_type is not None and value is not None and valid_type != value.resource_type:
            raise ValueError(f"value must be '{valid_type}'")
        return value


ResourceT = TypeVar("ResourceT", bound=Resource)


class ListResponse(Generic[ResourceT], Response):
    """List response."""

    schemas: FrozenSet[str]
    total_results: int = Field(alias="totalResults")
    start_index: Optional[int] = Field(alias="startIndex")
    items_per_page: Optional[int] = Field(alias="itemsPerPage")
    resources: Sequence[ResourceT] = Field((), alias="Resources")

    @pydantic.validator("schemas", pre=True)
    def check_schemas(cls, value: FrozenSet[str]) -> FrozenSet[str]:  # noqa: D102
        value = _fix_str_schemas(value)
        if "urn:ietf:params:scim:api:messages:2.0:ListResponse" not in value:
            raise ValueError("missing schema")
        return value
