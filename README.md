[![pipeline status](https://git.app.uib.no/it-bott-integrasjoner/scim2-client/badges/master/pipeline.svg)](https://git.app.uib.no/it-bott-integrasjoner/scim2-client/-/commits/master) 
[![coverage report](https://git.app.uib.no/it-bott-integrasjoner/scim2-client/badges/master/coverage.svg)](https://git.app.uib.no/it-bott-integrasjoner/scim2-client/-/commits/master)

# scim2-client

A generic <abbr title="System for Cross-domain Identity Management">SCIM</abbr> version 2 client.  See [RFC7644 – Protocol](https://datatracker.ietf.org/doc/html/rfc7644)
and [RFC7643 – Core Schema](https://datatracker.ietf.org/doc/html/rfc7643).

Anything outside the scope of the SCIM RFCs must be implemented elsewhere.

# Examples

See [./examples](./examples)

See [./tests](./tests) for more examples.
