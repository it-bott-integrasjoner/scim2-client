import json
from json import JSONDecodeError
from typing import Any, Optional, Type

import pytest
import requests
import requests_mock
from pydantic import SecretStr, ValidationError, parse_obj_as, HttpUrl
from requests import HTTPError

from scim2_client import (
    Config,
    Client,
    ScimError,
    Endpoints,
    user_query_request,
    user_request,
    User,
    QueryParameters,
    AttributesParameter,
    ItemRequest,
    QueryRequest,
    ListResponse,
    ErrorResponse,
    ResourceResponse,
    Request,
    eq,
    Resource,
)
from scim2_client.client import UnhandledResponseError
from tests.conftest import now, ExceptionT


def test_config(example_config_json: Any) -> None:
    config = Config(**example_config_json)

    assert config.endpoints.users == "Users"
    assert config.endpoints.groups == "Groups"
    assert config.endpoints.base_url == "https://example.com/scim/v2"
    for secret in config.headers.values():
        assert isinstance(secret, SecretStr)


@pytest.mark.parametrize(
    argnames=["url"],
    argvalues=[
        ["https://example.com/#fragment"],
        ["https://example.com/?query"],
        ["https://example.com/"],
    ],
)
def test_endpoints_invalid_base_url(url: str) -> None:
    assert parse_obj_as(HttpUrl, url) == url
    with pytest.raises(ValidationError) as exc:
        Endpoints(base_url=url)

    errors = exc.value.errors()

    assert len(errors) == 1
    assert errors[0]["loc"][0] == "base_url"


def test_client___init__(example_config: Any) -> None:
    client = Client(config=example_config)

    assert client.config is example_config
    assert isinstance(client.session, requests.Session)


def test_client___init___with_session(example_config: Any) -> None:
    session = requests.Session()
    client = Client(config=example_config, session=session)

    assert client.config is example_config
    assert client.session is session


def test_client___init___invalid_config() -> None:
    with pytest.raises(TypeError):
        Client(config=12)  # type: ignore


def test_client_get_user(example_client: Client, full_user: Any) -> None:
    user_id = full_user["id"]
    request = user_request(id=user_id)
    response = example_client(request)

    assert type(response) == ResourceResponse
    assert response.request is request
    assert type(response.payload) == User
    assert response.payload == User(**full_user)


def test_client___call__raw_response(example_client: Client, full_user: Any) -> None:
    user_id = full_user["id"]
    request = ItemRequest(
        request_type=Request.Type.USER,
        result_type=requests.Response,
        id=user_id,
    )
    response = example_client(request)

    assert type(response) == ResourceResponse
    assert response.request is request
    assert isinstance(response.payload, requests.Response)


@pytest.mark.parametrize(
    argnames=["parameters", "extra_parameters", "expected_parameters"],
    argvalues=[
        (None, {}, {}),
        (None, {"name": "Eigill"}, {"name": "Eigill"}),
        (None, {"active": True}, {"active": "true"}),
        (None, {"active": False}, {"active": "false"}),
        (
            None,
            {"meta.lastModified": now()},
            {"meta.lastModified": "2022-01-02T03:04:05Z"},
        ),
        (QueryParameters(), {}, {}),
        (
            QueryParameters(
                filter=eq("name", "Egill"),
                count=10,
                start_index=11,
                attributes=AttributesParameter(
                    include=False,
                    attributes=("name", "emails"),
                ),
                sort_by="name",
                sort_order=QueryParameters.SortOrder.ASCENDING,
            ),
            {},
            {
                "count": 10,
                "excludedAttributes": "name,emails",
                "filter": 'name eq "Egill"',
                "startIndex": 11,
                "sortBy": "name",
                "sortOrder": "ascending",
            },
        ),
    ],
)
def test_client_query_users(
    example_client: Client,
    users: Any,
    full_user: Any,
    mock_api: requests_mock.Mocker,
    parameters: Optional[QueryParameters],
    extra_parameters: dict,
    expected_parameters: dict,
) -> None:
    mock_api.get("https://example.com/scim/v2/Users", json=users)
    request: QueryRequest[ListResponse[User]] = user_query_request(
        parameters=parameters, extra_parameters=extra_parameters
    )
    response: ResourceResponse[ListResponse[User]] = example_client(request)

    assert type(response) == ResourceResponse
    assert response.request is request
    assert request.query_params == expected_parameters
    assert isinstance(response.payload, ListResponse)
    assert json.loads(response.payload.json(by_alias=True, exclude_unset=True)) == users


def test_client_query_users_no_attributes(
    example_client: Client,
    minimal_users: Any,
    minimal_users_schemas_is_str: Any,
    mock_api: requests_mock.Mocker,
) -> None:
    mock_api.get(
        "https://example.com/scim/v2/Users?attributes=",
        json=minimal_users_schemas_is_str,
        complete_qs=True,
    )
    request: QueryRequest[ListResponse[User]] = user_query_request(
        extra_parameters={"attributes": ""},
    )
    response: ResourceResponse[ListResponse[User]] = example_client(request)

    assert type(response) == ResourceResponse
    assert response.request is request
    assert isinstance(response.payload, ListResponse)
    assert type(response.payload.resources[0]) == User
    assert (
        json.loads(response.payload.json(by_alias=True, exclude_unset=True, exclude_defaults=True))
        == minimal_users
    )


def test_client_query_groups(example_client: Client, groups: Any) -> None:
    result_type = ListResponse
    parameters = QueryParameters()
    request: QueryRequest[ListResponse[Any]] = QueryRequest(
        request_type=Request.Type.GROUP,
        result_type=result_type,
        parameters=parameters,
    )
    response: ResourceResponse[ListResponse[Resource]] = example_client(request)

    assert type(response) == ResourceResponse
    assert response.request is request
    assert type(response.payload) == result_type
    assert response.payload == result_type(**groups)
    assert len(response.payload.resources) == 1
    assert type(response.payload.resources[0]) == Resource


def test_client_call_error_response(example_client: Client, invalid_filter: Any) -> None:
    filter_ = eq("givenName", "Egil").value_path("name")
    request = QueryRequest(
        request_type=Request.Type.USER,
        result_type=ListResponse,
        parameters=QueryParameters(filter=filter_),
    )

    with pytest.raises(ScimError) as exc:
        example_client(request)
    response: ErrorResponse = exc.value.response

    assert exc.value.request is request
    assert isinstance(response, ErrorResponse)
    assert response == ErrorResponse(**invalid_filter)
    assert json.loads(response.json(by_alias=True)) == invalid_filter


def test_client_call_unknown_error(example_client: Client, mock_api: requests_mock.Mocker) -> None:
    request = user_request(id="12")
    status_code = 418
    mock_api.request(
        method=request.method,
        url="https://example.com/scim/v2/Users/12",
        status_code=status_code,
        json="null",
    )
    with pytest.raises(HTTPError) as exc:
        example_client(request)

    assert exc.value.response.status_code == status_code


def test_client_call_unknown_response(
    example_client: Client, mock_api: requests_mock.Mocker
) -> None:
    request = user_request(id="12")
    status_code = 600
    mock_api.request(
        method=request.method,
        url="https://example.com/scim/v2/Users/12",
        status_code=status_code,
        json="null",
    )
    with pytest.raises(UnhandledResponseError) as exc:
        example_client(request)

    assert str(exc.value) == f"Unhandled response, status code {status_code}"


@pytest.mark.parametrize(
    argnames=["status_code", "exception_class"],
    argvalues=[
        [200, JSONDecodeError],
        [502, HTTPError],
        [600, UnhandledResponseError],
    ],
)
def test_client_call_invalid_json(
    example_client: Client,
    mock_api: requests_mock.Mocker,
    status_code: int,
    exception_class: Type[ExceptionT],
) -> None:
    request = user_request(id="12")
    mock_api.request(
        method=request.method,
        url="https://example.com/scim/v2/Users/12",
        status_code=status_code,
        text="<html><title>502 Bad Gateway</title></html>",
    )
    with pytest.raises(exception_class):
        example_client(request)


def test_iter_resources(
    example_config: Config,
    mock_api: requests_mock.Mocker,
    minimal_users_page_1: Any,
    minimal_users_page_2: Any,
) -> None:
    client = Client(config=example_config)
    mock_api.get(
        "https://example.com/scim/v2/Users?startIndex=2",
        json=minimal_users_page_2,
        complete_qs=True,
    )
    mock_api.get(
        "https://example.com/scim/v2/Users",
        json=minimal_users_page_1,
        complete_qs=True,
    )
    assert minimal_users_page_1["totalResults"] == minimal_users_page_2["totalResults"] == 2

    total_results = 0
    user: User
    for user in client.iter_resources(user_query_request()):
        assert isinstance(user, User)
        total_results += 1

    assert total_results == 2


def test_iter_resources_no_resources(
    example_config: Config,
    mock_api: requests_mock.Mocker,
    empty_list_response: Any,
) -> None:
    client = Client(config=example_config)
    mock_api.get(
        "https://example.com/scim/v2/Users",
        json=empty_list_response,
        complete_qs=True,
    )

    xs = tuple(client.iter_resources(user_query_request()))

    assert xs == ()


def test_iter_resources_single_page(
    example_config: Config,
    mock_api: requests_mock.Mocker,
    minimal_users: Any,
) -> None:
    client = Client(config=example_config)
    mock_api.get(
        "https://example.com/scim/v2/Users",
        json=minimal_users,
        complete_qs=True,
    )

    total_results = 0
    user: User
    for user in client.iter_resources(user_query_request()):
        assert isinstance(user, User)
        total_results += 1

    assert total_results == 1


def test_resource_response_next_request_unsupported_request(empty_list_response: Any) -> None:
    response: Any = ResourceResponse(
        payload=ListResponse(**empty_list_response), request=user_request(id="12")
    )
    with pytest.raises(NotImplementedError) as exc:
        response.next_request

    assert exc.value.args[0].startswith("Not implemented for request type")


def test_resource_response_next_request_unsupported_payload() -> None:
    response: Any = ResourceResponse(payload=Resource(id="12"), request=user_query_request())
    with pytest.raises(NotImplementedError) as exc:
        response.next_request

    assert exc.value.args[0].startswith("Not implemented for payload type")


def test_resource_response_next_request_empty_response(empty_list_response: Any) -> None:
    response: Any = ResourceResponse(
        payload=ListResponse(**empty_list_response), request=user_query_request()
    )

    assert response.next_request is None


def test_resource_response_next_request_empty_page(empty_list_response: Any) -> None:
    response: Any = ResourceResponse(
        payload=ListResponse(**empty_list_response, total_results=10, start_index=10),
        request=user_query_request(),
    )

    assert response.next_request is None
