from typing import Any

import pytest
from pydantic import ValidationError

from scim2_client import User


def test_user_meta_not_present(full_user: Any) -> None:
    del full_user["meta"]
    User(**full_user)


def test_user_invalid_resource_type(groups: Any) -> None:
    with pytest.raises(ValidationError) as exc:
        User(**groups["Resources"][0])

    assert exc.value.errors() == [
        {"loc": ("meta",), "msg": "value must be 'User'", "type": "value_error"}
    ]
