import pytest

from scim2_client import ItemRequest, Request


def test_item_request_empty_id() -> None:
    with pytest.raises(ValueError) as exc:
        ItemRequest(request_type=Request.Type.USER, result_type=dict, id="")

    assert str(exc.value) == "id cannot be empty"


def test_item_request_invalid_request_type() -> None:
    with pytest.raises(TypeError) as exc:
        ItemRequest(request_type=1, result_type=dict, id="123")  # type: ignore[arg-type]

    assert str(exc.value) == "Invalid request type '1'"


def test_item_request_invalid_result_type() -> None:
    with pytest.raises(TypeError) as exc:
        ItemRequest(request_type=Request.Type.USER, result_type="", id="123")  # type: ignore[arg-type]

    assert str(exc.value) == "result_type can't be empty"
