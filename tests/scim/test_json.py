import json

import pytest

from scim2_client.scim.json import ScimJSONEncoder
from tests.conftest import now


def test_serialize_datetime() -> None:
    value = now()
    assert json.dumps(value, cls=ScimJSONEncoder) == '"2022-01-02T03:04:05Z"'


def test_serialize_unknown() -> None:
    value = object()
    with pytest.raises(TypeError):
        json.dumps(value, cls=ScimJSONEncoder)
