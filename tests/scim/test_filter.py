from scim2_client import (
    eq,
    ne,
    co,
    sw,
    ew,
    gt,
    ge,
    lt,
    le,
    pr,
    LessThanOrEqual,
    Equal,
)
from tests.conftest import now


def test_dsl() -> None:
    # This should cover everything
    filter_ = str(
        pr("a")
        .and_(eq("b", None))
        .and_(ne("c", "d").or_(co("e", "f")).group())
        .and_(sw("g", "h").or_(ew("i", '"j"')).not_())
        .and_(gt("k", "l"))
        .and_(ge("m", False))
        .and_(lt("n", True))
        .and_(le("o", 12))
        .and_(eq("p", now()))
        .value_path("q")
        .and_(LessThanOrEqual(name="namespace:r", value="s"))
        .or_(Equal(name="t", value="u"))
    )
    assert (
        filter_
        == 'q[a pr and b eq null and (c ne "d" or e co "f") and not(g sw "h" or i ew "\\"j\\"")'
        ' and k gt "l" and m ge false and n lt true and o le 12 and p eq "2022-01-02T03:04:05Z"]'
        ' and namespace:r le "s" or t eq "u"'
    )
