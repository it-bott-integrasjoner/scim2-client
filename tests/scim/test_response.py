from typing import Any

import pytest
from pydantic import ValidationError

from scim2_client import ListResponse, ErrorResponse


def test_list_response_missing_schema(users: Any) -> None:
    with pytest.raises(ValidationError) as exc:
        ListResponse(**{**users, "schemas": []})

    assert ListResponse(**users)
    assert exc.value.errors() == [
        {"loc": ("schemas",), "msg": "missing schema", "type": "value_error"}
    ]


def test_error_response_missing_schema(invalid_filter: Any) -> None:
    with pytest.raises(ValidationError) as exc:
        ErrorResponse(**{**invalid_filter, "schemas": []})

    assert ErrorResponse(**invalid_filter)
    assert exc.value.errors() == [
        {"loc": ("schemas",), "msg": "missing schema", "type": "value_error"}
    ]
