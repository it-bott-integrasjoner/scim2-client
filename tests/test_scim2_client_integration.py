import json
import os
from typing import Iterable

import pytest

from scim2_client import (
    Config,
    Client,
    QueryParameters,
    QueryRequest,
    Resource,
    Filter,
    Request,
    ItemRequest,
    user_query_request,
    user_request,
    User,
    ListResponse,
    eq,
    pr,
    ne,
    lt,
    le,
    gt,
    ge,
    sw,
    ew,
    co,
    Present,
    AttributesParameter,
    ResourceResponse,
)
from tests.conftest import now


@pytest.fixture
def integration_config() -> Config:
    path = os.environ.get("SCIM_CLIENT_INTEGRATION_CONFIG")
    if not path:
        # It may be tempting to use a magic file path here, but it's best to be explicit
        raise AssertionError(
            "Set SCIM_CLIENT_INTEGRATION_CONFIG environment variable to config file"
        )
    with open(path, encoding="utf-8") as f:
        return Config(**json.load(f))


@pytest.fixture
def client(integration_config: Config) -> Client:
    return Client(config=integration_config)


@pytest.mark.integration
def test_client_users(client: Client) -> None:
    result_type = User
    params = QueryParameters(count=1)
    request: QueryRequest[ListResponse[User]] = user_query_request(parameters=params)
    assert isinstance(request, QueryRequest)
    assert request.parameters == params
    response: ResourceResponse = client(request)
    payload: ListResponse[User] = response.payload

    assert isinstance(payload, ListResponse)
    assert "urn:ietf:params:scim:schemas:core:2.0:User" in payload.resources[0].schemas  # type: ignore[operator]
    assert payload.total_results > 1
    assert payload.items_per_page == 1

    single_request = user_request(id=payload.resources[0].id)
    single_response: ResourceResponse[User] = client(single_request)
    single_payload: User = single_response.payload

    assert single_payload == payload.resources[0]
    assert type(single_payload) == result_type


@pytest.mark.integration
def test_client_groups(client: Client) -> None:
    request_type = Request.Type.GROUP
    result_type = Resource
    params = QueryParameters(count=1)
    request: QueryRequest[ListResponse[Resource]] = QueryRequest(
        request_type=request_type,
        result_type=ListResponse,
        parameters=params,
    )
    assert isinstance(request, QueryRequest)
    assert request.parameters == params
    response: ResourceResponse[ListResponse[Resource]] = client(request)
    payload: ListResponse[Resource] = response.payload

    assert type(payload) == ListResponse
    assert type(payload.resources[0]) == result_type
    assert "urn:ietf:params:scim:schemas:core:2.0:Group" in payload.resources[0].schemas  # type: ignore[operator]
    assert payload.total_results > 1
    assert payload.items_per_page == 1

    single_request: ItemRequest[Resource] = ItemRequest(
        request_type=request_type,
        result_type=result_type,
        id=payload.resources[0].id,
    )
    single_response: ResourceResponse[Resource] = client(single_request)
    single_payload: Resource = single_response.payload

    assert type(single_payload) == result_type
    assert single_payload == payload.resources[0]


@pytest.mark.integration
@pytest.mark.parametrize(
    argnames=["query_filter"],
    argvalues=[
        [eq("meta.resourceType", "User")],
        [ne("meta.resourceType", "Skallagrim")],
        [
            eq("resourceType", "Skallagrim")
            .or_(Present("U"))
            .and_(pr("P"))
            .not_()
            .value_path("meta")
            .and_(pr("U").or_(Present("P")).group())
        ],
        [co("meta.resourceType", "se")],
        [sw("meta.resourceType", "Us")],
        [ew("meta.resourceType", "er")],
        [pr("id")],
        [gt("meta.lastModified", now())],
        [ge("meta.lastModified", now())],
        [lt("meta.lastModified", now())],
        [le("meta.lastModified", now())],
        [eq("resourceType", "User").value_path("meta")],
        [eq("meta.resourceType", "User").and_(pr("id"))],
    ],
)
def test_client_query_users_with_filter(client: Client, query_filter: Filter) -> None:
    # Set count to 1, so we don't retrieve more than we need
    request: QueryRequest[ListResponse[User]] = user_query_request(
        parameters=QueryParameters(count=1, filter=query_filter)
    )
    response: ResourceResponse[ListResponse[User]] = client(request)

    assert isinstance(response.payload, ListResponse)
    assert response.payload.items_per_page == 1


@pytest.mark.integration
@pytest.mark.parametrize(
    argnames=["attributes", "include"],
    argvalues=[
        [{"name"}, True],
        [{"schemas", "meta"}, True],
        [{"name"}, False],
    ],
)
def test_client_query_users_with_attributes(
    client: Client,
    attributes: Iterable[str],
    include: bool,
) -> None:
    # Set count to 1, so we don't retrieve more than we need
    parameters = QueryParameters(count=1, attributes=AttributesParameter(attributes, include))

    request: QueryRequest[ListResponse[User]] = user_query_request(parameters=parameters)
    response: ResourceResponse[ListResponse[User]] = client(request)

    assert isinstance(response.payload, ListResponse)
    assert response.payload.items_per_page == 1


@pytest.mark.integration
def test_client_query_users_with_extra_parameters(client: Client) -> None:
    # Set count to 1, so we don't retrieve more than we need
    request: QueryRequest[ListResponse[User]] = user_query_request(
        parameters=QueryParameters(count=1), extra_parameters=dict(active=False)
    )
    response: ResourceResponse[ListResponse[User]] = client(request)

    assert isinstance(response.payload, ListResponse)
    assert response.payload.items_per_page == 1


@pytest.mark.integration
@pytest.mark.parametrize(
    argnames=["sort_order"],
    argvalues=[[x] for x in QueryParameters.SortOrder],
)
def test_client_query_users_sort(
    sort_order: QueryParameters.SortOrder,
    client: Client,
) -> None:
    # Set count to 1, so we don't retrieve more than we need
    parameters = QueryParameters(
        count=1,
        sort_by="displayName",
        sort_order=sort_order,
        attributes=AttributesParameter(attributes=("displayName",)),
    )

    request: QueryRequest[ListResponse[User]] = user_query_request(parameters=parameters)
    response: ResourceResponse[ListResponse[User]] = client(request)

    assert isinstance(response.payload, ListResponse)
    assert response.payload.items_per_page == 1
