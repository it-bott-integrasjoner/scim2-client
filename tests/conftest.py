from __future__ import annotations

import datetime
import json
import pathlib
from os import PathLike
from typing import TextIO, Union, Any, Iterator, TypeVar

import pytest
import requests_mock

from scim2_client import Config, Client

ExceptionT = TypeVar("ExceptionT", bound=BaseException)


def resolve_path(path: Union[str, PathLike[str]]) -> pathlib.Path:
    return pathlib.Path(pathlib.Path(__file__).parent, path)


def open_file(path: Union[str, PathLike[str]], **open_kwargs: Any) -> TextIO:
    """Opens a file relative to this file."""
    return resolve_path(path).open(**open_kwargs)


@pytest.fixture
def example_config_json() -> Any:
    with open_file("../config.example.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def example_config(example_config_json: Any) -> Config:
    return Config(**example_config_json)


@pytest.fixture
def example_client(
    example_config: Config,
    mock_api: requests_mock.Mocker,
    users: Any,
    full_user: Any,
    groups: Any,
    invalid_filter: Any,
) -> Client:
    mock_api.get("https://example.com/scim/v2/Users", json=users, complete_qs=True)
    mock_api.get(
        f"https://example.com/scim/v2/Users/{full_user['id']}", json=full_user, complete_qs=True
    )
    for u in users["Resources"]:
        mock_api.get(
            full_user["meta"]["location"],
            json=u,
            complete_qs=True,
        )
    mock_api.get(
        "https://example.com/scim/v2/Users?filter=name%5BgivenName+eq+%22Egil%22%5D",
        json=invalid_filter,
        status_code=400,
        complete_qs=True,
    )
    mock_api.get("https://example.com/scim/v2/Groups", json=groups)
    for group in groups["Resources"]:
        mock_api.get(
            group["meta"]["location"],
            json=group,
            complete_qs=True,
        )
    return Client(config=example_config)


@pytest.fixture
def mock_api() -> Iterator[requests_mock.Mocker]:
    with requests_mock.Mocker() as m:
        yield m


@pytest.fixture
def users() -> Any:
    with open_file("fixtures/users.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def full_user(users: Any) -> Any:
    with open_file("fixtures/full_user.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def minimal_users() -> Any:
    with open_file("fixtures/minimal_users.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def minimal_users_page_1() -> Any:
    with open_file("fixtures/minimal_users_page_1.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def minimal_users_page_2() -> Any:
    with open_file("fixtures/minimal_users_page_2.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def empty_list_response() -> Any:
    with open_file("fixtures/empty_list_response.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def minimal_users_schemas_is_str(minimal_users: Any) -> Any:
    with open_file("fixtures/minimal_users_schemas_is_str.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def groups() -> Any:
    with open_file("fixtures/groups.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


@pytest.fixture
def invalid_filter() -> Any:
    with open_file("fixtures/invalid_filter.json", encoding="utf-8") as jsonfile:
        return json.load(jsonfile)


def now() -> datetime.datetime:
    return datetime.datetime(2022, 1, 2, 3, 4, 5, tzinfo=datetime.timezone.utc)


def pytest_collection_modifyitems(config, items) -> Any:  # type: ignore[no-untyped-def]  # pragma no cover
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(reason='Not running with pytest -m "integration"')
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)
